package pweb2;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.*;

@SuppressWarnings("serial")
public class EditorServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		@SuppressWarnings("unchecked")
		HashMap<String,Usuario> datos=(HashMap<String, Usuario>)getServletContext().getAttribute("datos");
		String dni=req.getParameter("dni");
		String nombres=req.getParameter("nombres");
		String apellidos=req.getParameter("apellidos");
		String departamento=req.getParameter("departamento");
		Usuario v=datos.get(dni);
		v.setapellido(apellidos);
		v.setdepartamento(departamento);
		v.setname(nombres);
		datos.remove(dni);
		datos.put(dni, v);
        String dire="index.jsp";
        resp.sendRedirect("http://www.proyectopw2-166501.com/"+dire);
	}	
}
