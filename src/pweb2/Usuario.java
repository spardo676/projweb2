package pweb2;

public class Usuario {
	private String dni;
	private String name;
	private String apellido;
	private String departamento;
	public Usuario(String newDNI,String newName,String newApellido,String newDepartamento)
	{
		dni=newDNI;
		name=newName;
		apellido=newApellido;
		departamento=newDepartamento;
	}
	public String getdni()
	{
		return dni;
	}
	public String getname()
	{
		return name;
	}
	public String getapellido()
	{
		return apellido;
	}
	public String getdepartamento()
	{
		return departamento;
	}

	public void  setname(String newName)
	{
		name=newName;
	}
	public void setapellido(String newApellido)
	{
		apellido=newApellido;
	}
	public void setdepartamento(String newDepartamento)
	{
		departamento=newDepartamento;
	}


}