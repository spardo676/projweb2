package pweb2;

import java.io.*;
import java.util.HashMap;

import javax.servlet.http.*;

@SuppressWarnings("serial")
public class UsuarioServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		String dni=req.getParameter("dni");
		String nombres=req.getParameter("nombres");
		String apellidos=req.getParameter("apellidos");
		String departamento=req.getParameter("departamento");
		Usuario v=new Usuario(dni,nombres,apellidos,departamento);
		if(getServletContext().getAttribute("datos")!=null){

			@SuppressWarnings("unchecked")
			HashMap<String,Usuario> datos=(HashMap<String, Usuario>)getServletContext().getAttribute("datos");
			if(datos.get(dni)==null)
				datos.put(dni, v);
		}
		else
		{
			HashMap<String,Usuario> datos=new HashMap<String, Usuario>();
			datos.put(dni, v);
			getServletContext().setAttribute("datos", datos);
			resp.getWriter().println("Hello, world "+datos.size());
			resp.setContentType("text/html");
		}
        String dire="index.jsp";
        resp.sendRedirect("http://www.proyectopw2-166501.appspot.com/"+dire);
	}
}